myappend_39522674(A,B,C) :- call_with_time_limit(20, myappend(A,B,C)).

:- begin_tests(popl2).

test(a_2_01) :- myappend_39522674([],[],R), R=[].
test(a_2_02) :- myappend_39522674([1],[],R), R=[1].
test(a_2_03) :- myappend_39522674([1],[2],R), R=[1,2].
test(a_2_04) :- myappend_39522674([],[1,2,3,4],R), R=[1,2,3,4].
test(a_2_05) :- myappend_39522674([1],[2,3,4],R), R=[1,2,3,4].
test(a_2_06) :- myappend_39522674([1,2],[3,4],R), R=[1,2,3,4].
test(a_2_07) :- myappend_39522674([1,2,3],[4],R), R=[1,2,3,4].
test(a_2_08) :- myappend_39522674([1,2,3,4],[],R), R=[1,2,3,4].

:- end_tests(popl2).
