
for std in `ls -d */`
do
  cd $std

  name=`echo $std | rev | colrm 1 1 | rev`
  
  grep 'a_._' output.?.txt \
    | grep -v "Test succeeded with choicepoint" \
    | awk '{ print $3 "," substr($0, index($0,$4)) }' \
    | sed 's/:,/,/g' \
    | sed 's/^/'"$name"',/g' \
    | sed 's/,a_\(.\)_/,\1,a_\1_/g' \
    | sed 's/received error: time:run_alarm_goal\/2: Undefined procedure: .*\/./not_implemented/g' \
    | sed 's/received error: Out of global stack/out_of_stack/g' \
    | sed 's/received error: Out of local stack/out_of_stack/g' \
    | sed 's/received error: \(.*\)/received_error,\1/g' \
    > errors.csv
  
  cd ..
done

echo "name,exercise,test,error,misc" > errors.csv
cat */errors.csv >> errors.csv
