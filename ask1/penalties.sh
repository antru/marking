echo "name,exercise,penalty" > penalties.csv

for std in `ls -d */`
do
  name=`echo $std | rev | colrm 1 1 | rev`
  echo $name',1,1' >> penalties.csv
  echo $name',2,1' >> penalties.csv
  echo $name',3,1' >> penalties.csv
  echo $name',4,1' >> penalties.csv
  echo $name',5,1' >> penalties.csv
done

