
for std in `ls -d */`
do
  cd $std
  
  echo Entering $std
  
  echo run_tests. | swipl -l *.pl ../test1.pl &> output.1.txt
  echo run_tests. | swipl -l *.pl ../test2.pl &> output.2.txt
  echo run_tests. | swipl -l *.pl ../test3.pl &> output.3.txt
  echo run_tests. | swipl -l *.pl ../test4.pl &> output.4.txt
  echo run_tests. | swipl -l *.pl ../test5.pl &> output.5.txt
  
  cd ..
done
