mymax([X|Xs], R):- mymax2(Xs, X, R).
mymax2([], R, R).
mymax2([X|Xs], WK, R):- X >  WK, mymax2(Xs, X, R).
mymax2([X|Xs], WK, R):- X =< WK, mymax2(Xs, WK, R).
