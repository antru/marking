mymax_27df9084(A,B) :- call_with_time_limit(20, mymax(A,B)).

:- begin_tests(popl1).

test(a_1_01) :- mymax_27df9084([1], M), M=1.
test(a_1_02) :- mymax_27df9084([2,2,2,2], M), M=2.
test(a_1_03) :- mymax_27df9084([1,2,3,4], M), M=4.
test(a_1_04) :- mymax_27df9084([4,3,2,1], M), M=4.
test(a_1_05) :- mymax_27df9084([1,9,1,9], M), M=9.

:- end_tests(popl1).
