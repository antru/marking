
echo "test;weight" > weights.csv

for file in $*
do
  echo Processing $file "..."
  
  N=`grep 'a_._' $file | wc -l`  # all tests of a specific exercize
  W=`bc <<< 'scale=2; 100/'$N`   # base weight
  
  grep 'a_._' $file | sed 's/test(\(.*\)) :- .*$/\1;'$W'/g' >> weights.csv
  
done