# README #

Various scripts used for marking and grading for Principles of Programming Languages Course at dit@UoA.

### Dependencies ###

* swi prolog
* ghci with HUnit module
* csvkit

### Instructions ###

For simple instructions, check the README for each assignment.

### Contact ###

For questions, comments, errors or suggestions contact me at antru [at] di.uoa.gr