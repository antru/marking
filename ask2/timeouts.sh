
for std in `ls -d */`
do
  cd $std
  
  for i in `seq 5`
  do
    cat output.$i.txt \
      | tr '\n' '\f' \
      | sed 's/Failure in: \([0-9]*:test_[0-9]*_[0-9]*\)\fexpected: Nothing/Timeout in: \1\fexpected: Nothing/g' \
      | tr '\f' '\n' \
      > tmp.txt
    mv tmp.txt output.$i.txt
  done

  cd ..
done
