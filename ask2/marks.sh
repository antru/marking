
csvsql --query "select 
                  errors.name, 
                  errors.exercise, 
                  errors.test, 
                  errors.error, 
                  weights.weight as test_weight,
                  error_types.weight as error_weight
                from errors, weights, error_types
                where 
                  errors.test=weights.test and 
                  errors.error=error_types.error" \
errors.csv weights.csv error_types.csv > marks_full.csv
       
       
csvsql --query "select 
                  penalties.name, 
                  penalties.exercise,
                  ifnull(
		    (100-sum(test_weight*error_weight))*penalty,
		    100.0*penalty) as mark
                from penalties
                left join marks_full
                on 
                  marks_full.name=penalties.name and 
                  marks_full.exercise=penalties.exercise
                group by penalties.name, penalties.exercise" \
marks_full.csv penalties.csv > tmp.csv
 

csvsql --query "select distinct
                  tmpex1.name,
                  email,
                  cast(tmpex1.mark as int) as ex1,
                  cast(tmpex2.mark as int) as ex2,
                  cast(tmpex3.mark as int) as ex3,
                  cast(tmpex4.mark as int) as ex4,
                  cast(tmpex5.mark as int) as ex5,
                  cast(tmptot.mean as int) as total
                from
		  (select * from tmp where exercise=1) as tmpex1,
		  (select * from tmp where exercise=2) as tmpex2,
		  (select * from tmp where exercise=3) as tmpex3,
		  (select * from tmp where exercise=4) as tmpex4,
		  (select * from tmp where exercise=5) as tmpex5,
		  (select name, avg(mark) as mean from tmp group by name) as tmptot,
		  emails
                where 
                  tmpex1.name=tmpex2.name and
                  tmpex2.name=tmpex3.name and
                  tmpex3.name=tmpex4.name and
                  tmpex4.name=tmpex5.name and
                  tmpex5.name=tmptot.name and
                  tmptot.name=emails.name" \
tmp.csv emails.csv > marks.csv
