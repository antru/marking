echo "name,exercise,penalty" > penalties.csv

for std in `ls -d */`
do
  name=`echo $std | rev | colrm 1 1 | rev`
  
  for ex in `seq 5`
  do
    flag=`grep 'Not in scope' $name/output.$ex.txt`
    if [ -z "$flag" ]
    then
      echo $name','$ex',1' >> penalties.csv
    else
      echo $name','$ex',0' >> penalties.csv
    fi
  done
done

