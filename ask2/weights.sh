
echo "test,weight" > weights.csv

for file in $*
do
  echo Processing $file "..."
  
  N=`grep '^test_._' $file | wc -l`  # all tests of a specific exercize
  W=`bc <<< 'scale=2; 100/'$N`   # base weight
  
  grep '^test_._' $file | awk -v weight=$W '{ print $1 "," weight }' >> weights.csv
  
done
