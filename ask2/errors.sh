
for std in `ls -d */`
do
  cd $std

  name=`echo $std | rev | colrm 1 1 | rev`
  
  cat output.?.txt | tr "\r" "\n" | grep '###' \
    | tr ":" " " \
    | awk '{ print $5 "," $2 }' \
    | sed 's/^/'"$name"',/g' \
    | sed 's/,test_\(.\)_/,\1,test_\1_/g' \
    > errors.csv
  
  cd ..
done

echo "name,exercise,test,error" > errors.csv
cat */errors.csv >> errors.csv
