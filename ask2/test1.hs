module MyTestsEx1 where 
import Main
import Test.HUnit
import System.IO
import System.Timeout

timed exp = timeout (10 * 10^6) $ return $! exp
assertTimedEqual s x y = timed x >>= \n -> assertEqual s n (Just y)

test_1_01 = TestCase (assertTimedEqual "" (mymax [1]) 1)
test_1_02 = TestCase (assertTimedEqual "" (mymax [2,2,2,2]) 2)
test_1_03 = TestCase (assertTimedEqual "" (mymax [1,2,3,4]) 4)
test_1_04 = TestCase (assertTimedEqual "" (mymax [4,3,2,1]) 4)
test_1_05 = TestCase (assertTimedEqual "" (mymax [1,9,1,9]) 9)

tests = TestList [ 
  TestLabel "test_1_01" test_1_01, 
  TestLabel "test_1_02" test_1_02, 
  TestLabel "test_1_03" test_1_03, 
  TestLabel "test_1_04" test_1_04, 
  TestLabel "test_1_05" test_1_05 ]

main = runTestTT tests
