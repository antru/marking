module MyTestsEx2 where 
import Main
import Test.HUnit
import System.IO
import System.Timeout

timed exp = timeout (10 * 10^6) $ return $! exp
assertTimedEqual s x y = timed x >>= \n -> assertEqual s n (Just y)

test_2_01 = TestCase (assertTimedEqual "" (mysum []) 0)
test_2_02 = TestCase (assertTimedEqual "" (mysum [1]) 1)
test_2_03 = TestCase (assertTimedEqual "" (mysum [2]) 2)
test_2_04 = TestCase (assertTimedEqual "" (mysum [1,2]) 3) 
test_2_05 = TestCase (assertTimedEqual "" (mysum [1,2,3]) 6)
test_2_06 = TestCase (assertTimedEqual "" (mysum [1,2,3,4]) 10) 

tests = TestList [ 
  TestLabel "test_2_01" test_2_01, 
  TestLabel "test_2_02" test_2_02, 
  TestLabel "test_2_03" test_2_03, 
  TestLabel "test_2_04" test_2_04, 
  TestLabel "test_2_05" test_2_05, 
  TestLabel "test_2_06" test_2_06 ]


main = runTestTT tests
