
for std in `ls -d */`
do
  cd $std
  
  echo Entering $std
  
  echo MyTestsEx1.main  | ghci *.hs ../test1.hs &> output.1.txt
  echo MyTestsEx2.main  | ghci *.hs ../test2.hs &> output.2.txt
  echo MyTestsEx3.main  | ghci *.hs ../test3.hs &> output.3.txt
  echo MyTestsEx4.main  | ghci *.hs ../test4.hs &> output.4.txt
  echo MyTestsEx5.main  | ghci *.hs ../test5.hs &> output.5.txt

  cd ..
done
