module MyTestsEx5 where 
import Main
import Test.HUnit
import System.IO
import System.Timeout

timed exp = timeout (10 * 10^6) $ return $! exp
assertTimedEqual s x y = timed x >>= \n -> assertEqual s n (Just y)

test_5_01 = TestCase (assertTimedEqual "" (myfun 1) 1)

tests = TestList [ 
  TestLabel "test_5_01" test_5_01  ]

main = runTestTT tests

